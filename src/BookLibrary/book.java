package BookLibrary;

import java.util.Scanner;

public class book {
    private int bookID;
    private String bookTitle;
    private int amount;
    private int available;
    Scanner input = new Scanner(System.in);

    public book() {
        super();
    }

    public book(int bookID,String bookTitle, int amount, int available){
        super();
        this.bookID = bookID;
        this.bookTitle = bookTitle;
        this.amount = amount;
        this.available = available;
    }

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public void method(int a,long b){
        System.out.println("int, long");
    }

    public void method(long a,int b){
        System.out.println("long, int");
    }

    public void addBook(){
        System.out.println("Book ID: ");
        bookID = input.nextInt();
        System.out.println("Book Title: ");
        bookTitle = input.next();
        System.out.println("Amount: ");
        amount = input.nextInt();
        System.out.println("Available: ");
        available = input.nextInt();
    }
    @Override
    public String toString() {
        return "Book ID: " +this.bookID+", Book Title: "+this.bookTitle+", Amount: "+this.amount+", Available: "+this.available;
    }

}
