package BookLibrary;
import java.util.Scanner;
public class program {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean cont = true;
        do {
            System.out.println("========== Main menu ========");
            System.out.println("1. Show library information");
            System.out.println("2. Add new book");
            System.out.println("3. Find book");
            System.out.println("4. Borrow a book");
            System.out.println("5. Return a book");
            System.out.println("6. Exit");
            System.out.println("=============================");
            System.out.println("Enter menu ID (1-6) :");
            int chon = input.nextInt();
            library library = new library();
            switch (chon) {
                case 1:
                    library.showBookInfo();
                    break;
                case 2:
                    library.addNewBook();
                    break;
                case 3:
                    library.findBook();
                    break;
                case 4:
                    library.borrowBook();
                    break;
                case 5:
                    library.returnBook();
                    break;
                default:
                    System.out.println("Goodbye!");
                    cont = false;
                    break;
            }
        } while (cont);
    }
}
