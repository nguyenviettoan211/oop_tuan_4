package BookLibrary;
import java.util.*;

public class library {
    private String libraryTitle;
    private static int nbBooks;
    public static ArrayList<book> arrbooks = new ArrayList<>();
    public static final int MAX_NUMBER_BOOKS = 100;
    Scanner input = new Scanner(System.in);

    public String getLibraryTitle() {
        return libraryTitle;
    }

    public void setLibraryTitle(String libraryTitle) {
        this.libraryTitle = libraryTitle;
    }

    public static int getNbBooks() {
        return nbBooks;
    }

    public static void setNbBooks(int nbBooks) {
        library.nbBooks = nbBooks;
    }


    public void addNewBook(){
        System.out.println("Enter the number of book: ");
        int soLuongSach = input.nextInt();
        int total = soLuongSach + nbBooks;
        if (total<MAX_NUMBER_BOOKS) {
            nbBooks += soLuongSach;
            for (int i=0;i<soLuongSach;i++){
                System.out.println("Enter the infomation of "+(i+1)+"'s book:");
                book b = new book();
                b.addBook();
                arrbooks.add(b);
            }
//            System.out.println(nbBooks);

        }
        else{
            System.out.println("Library has max number of books.");
        }
    }

    public void findBook(){
        System.out.println("Enter Book ID: ");
        int id = input.nextInt();
        boolean flag = true;
        for (int temp=0;temp <nbBooks;temp++){
            int a = arrbooks.get(temp).getBookID();
            if (a==id) {
                System.out.println("YES, it had!");
                System.out.println(arrbooks.get(temp).toString());
                System.out.println();
                flag = false;
            } else {
                flag = true;
            }
        }
        if (flag) {
            System.out.println("NO, It haven't!");
            System.out.println();
        }

    }

    public void showBookInfo() {
        for (int i=0; i<nbBooks; i++)
            System.out.println(arrbooks.get(i).toString());
        if (nbBooks==0)
            System.out.println("Library don't have any book!");
        System.out.println();
    }

    public void borrowBook(){
        System.out.println("Enter ID's Book you want: ");
        int idBorrow = input.nextInt();
            boolean flagBorrow = true;
            for (int temp = 0; temp < nbBooks; temp++) {
                int a = arrbooks.get(temp).getBookID();
                int available = arrbooks.get(temp).getAvailable();
                if (idBorrow == a && available > 0) {
                    int borrowBook = arrbooks.get(temp).getAvailable() - 1;
                    arrbooks.get(temp).setAvailable(borrowBook);
                    System.out.println("The book id borrowed successfully!");
                    flagBorrow = false;
                } else {
                    flagBorrow = true;
                }
            }
            if (flagBorrow)
                System.out.println("The book is not available!");
    }

    public void returnBook() {
            System.out.println("Enter ID's book return: ");
            int idReturn = input.nextInt();
            boolean flagReturn = true;
        for (int temp = 0; temp < nbBooks; temp++){
            int a = arrbooks.get(temp).getBookID();
            if(a == idReturn){
                int returnBook = arrbooks.get(temp).getAvailable() + 1;
                arrbooks.get(temp).setAvailable(returnBook);
                System.out.println("The book is returned successfully!");
                flagReturn = false;
            }
            else flagReturn = true;
        }
        if (flagReturn)
            System.out.println("Don't have that book!");
    }
}
